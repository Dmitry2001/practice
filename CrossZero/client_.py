from random import randint
from Cross_Zeroes_func import create_surf, cross_or_zero, show_current_situation_in_surf, is_end, save_in_history, \
    show_hist, protect
constant_of_surf = 3
create_surf(constant_of_surf=constant_of_surf)
player = None
ind = 0
while True:
    if ind == 0:
        if protect(3):
            row, col = protect(3)
            cross_or_zero(indicator=ind, position=[row+1, col+1])
        else:
            row_ai, col_ai = randint(1, 3), randint(1, 3)
            while cross_or_zero(indicator=ind, position=[row_ai, col_ai]) != True:
                row_ai, col_ai = randint(1, 3), randint(1, 3)
        print(f'---------------------------')
        show_current_situation_in_surf(constant_of_surf=constant_of_surf)
        print(f'---------------------------')
        if is_end(constant_of_surf=constant_of_surf):
            break
        save_in_history(current_player='0', position_step=[row_ai, col_ai])
        ind = (ind + 1) % 2
    if ind == 1:
        row, col = map(int, input('Введите позицию строка-столбец: ').split())
        while cross_or_zero(indicator=ind, position=[row, col]) != True:
            row, col = map(int, input().split())
        if is_end(constant_of_surf=constant_of_surf):
            break
        save_in_history(current_player='1', position_step=[row, col])
        ind = (ind + 1) % 2

ind_hist = (input('Показать историю игры? [Y/N]: '))

if ind_hist == 'Y':
    show_hist()
else:
    print('Thank you gor the game!')

