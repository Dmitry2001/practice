Buff = []
history_of_play = []
def create_surf(constant_of_surf):
    global Buff
    Buff = ['-'] * constant_of_surf
    for i in range(constant_of_surf):
        Buff[i] = ['-'] * constant_of_surf
def cross_or_zero(indicator, position):
    if Buff[position[0] - 1][position[1] - 1] is '-':
        if indicator == 1:
            Buff[position[0] - 1][position[1] - 1] = '+'
        else:
            Buff[position[0] - 1][position[1] - 1] = '0'
        return  True
    else:
        if indicator == 1:
            print('Это место уже занято, Миша! Попробуй ещё раз!')
        return  False
def is_end(constant_of_surf):
#диагональные проходы
    cross_main_diag,  zeroes_main_diag, cross_optional_diag, zeroes_optional_diag = 0, 0, 0, 0
    for i in range(constant_of_surf):
        if Buff[i][i] == '+':
            cross_main_diag += 1
        elif Buff[i][i] == '0':
            zeroes_main_diag += 1
        if Buff[i][constant_of_surf - 1 - i] == '+':
            cross_optional_diag += 1
        if Buff[i][constant_of_surf - 1 - i] == '0':
            zeroes_optional_diag += 1
    if cross_main_diag == constant_of_surf:
        print('Крестики победили!')
        return True
    if zeroes_main_diag == constant_of_surf:
        print('Нолики победили!')
        return True
    if cross_optional_diag == constant_of_surf:
        print('Крестики победили!')
        return True
    if zeroes_optional_diag == constant_of_surf:
        print('Нолики победили!')
        return True
#обычные проходы
    for i in range(constant_of_surf):
        zero_col = 0
        cross_col = 0
        for j in range(constant_of_surf):
            if Buff[i][j] == '+':
                cross_col += 1
            if Buff[i][j] == '0':
                zero_col += 1
        if cross_col == constant_of_surf:
            print("Крестики победили!")
            return True
        if zero_col == constant_of_surf:
            print("Нолики победили!")
            return True
    for j in range(constant_of_surf):
        cross_row = 0
        zero_row = 0
        for i in range(constant_of_surf):
            if Buff[i][j] == '+':
                cross_row += 1
            if Buff[i][j] == '0':
                zero_row += 1
        if cross_row == constant_of_surf:
            print("Крестики победили!")
            return True
        if zero_row == constant_of_surf:
            print("Нолики победили!")
            return True
    empty_sq = 0
    for row in Buff:
        for elem in row:
            if elem == '-':
                empty_sq += 1
    if empty_sq == 0:
        print('Ничья!')
        return True
def show_current_situation_in_surf(constant_of_surf):
    for i in range(constant_of_surf):
        for j in range(constant_of_surf):
            print(Buff[i][j], end = ' | ')
        print()
def save_in_history(**kwargs):
    history_of_play.append(kwargs)
def show_hist():
    for elem in history_of_play:
        print(elem)
def protect(constant_of_surf):
    diag = ''
    opt_diag = ''
    for i in range(constant_of_surf):
        row = ''
        col = ''
        diag += Buff[i][i]
        opt_diag += Buff[i][2 - i]
        for j in range(constant_of_surf):
            row += Buff[i][j]
            col += Buff[j][i]
        if row.count('+') == 2 and '-' in row:
            return [i, row.index('-') ]
        if col.count('+') == 2 and '-' in col:
            return [col.index('-'), i]
        if diag.count('+') == 2 and '-' in diag:
            return [diag.index('-'), diag.index('-')]
        if opt_diag.count('+') == 2 and '-' in opt_diag:
            return [opt_diag.index('-'), 2 - opt_diag.index('-')]
    # diag = ''
    # opt_diag = ''
    # for i in range(constant_of_surf):
    #     diag += Buff[i][i]
    #     opt_diag += Buff[i][2 - i]
    # if diag.count('+') == 2 and '-' in diag:
    #     return [diag.index('-'), diag.index('-')]
    # if opt_diag.count('+') == 2 and '-' in opt_diag:
    #     return [2 - diag.index('-'), diag.index('-')]




















