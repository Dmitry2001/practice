from Game_Nim.NIM_functions import put_stones, take_from_heap, show_heap, is_game_over
index_player = 1
current_player = None
#print('Введите количество куч: ')
count_of_heaps = int(input())
put_stones(count_of_heaps)
while True:
    if index_player % 2 == 1:
        current_player = 'Первый игрок'
        print(current_player)
        print('Текущая ситуация:')
        show_heap()
        print('Сколько будем брать: ')
        count_of_stones = int(input())
        print('Откуда будем брать: ')
        number_of_heap = int(input())
        if take_from_heap(heap_num=number_of_heap, count=count_of_stones):
            index_player += 1
        if is_game_over(winner=current_player):
            break
    else:
        current_player = 'Второй игрок'
        print(current_player)
        print('Текущая ситуация:')
        show_heap()
        print('Сколько будем брать: ')
        count_of_stones = int(input())
        print('Откуда будем брать: ')
        number_of_heap = int(input())
        if take_from_heap(heap_num=number_of_heap, count=count_of_stones):
            index_player += 1
        if is_game_over(winner=current_player):
            break


