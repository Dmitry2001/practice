from  random import randint
Buffer = []

def put_stones(number):
    for _ in range(number):
        Buffer.append(randint(1, number + 3))

def take_from_heap(heap_num, count):
    if count <= Buffer[heap_num - 1]:
        Buffer[heap_num - 1] -= count
        return True
    else:
        print('Ошибка! В куче меньше камней!')
        return False

def show_heap():
    print(Buffer)

def is_game_over(winner):
    if sum(Buffer) is 0:
        print("Поздравляю, " + winner + '.', "Вы выиграли!")
        return True

